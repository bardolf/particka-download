const puppeteer = require('puppeteer');
const exec = require('child_process').exec;

const YOUTUBE_DL_PATH = '/usr/bin/youtube-dl';
const CHROMIUM_BROWSER_PATH = '/usr/bin/chromium-browser';

const PLAY_VIDEO_BUTTON_SELECTOR = '.vjs-big-play-button';

function pad(num, size) {
    var s = num + "";
    while (s.length < size) {
        s = "0" + s;
    }
    return s;
}

async function download(part) {
    const browser = await puppeteer.launch({ headless: false, executablePath: CHROMIUM_BROWSER_PATH });
    const page = await browser.newPage();

    await page.goto('https://prima.iprima.cz/porady/particka/' + part + '-epizoda');
    var frames = await page.frames();
    var videoframe = frames.find(f => f.name().localeCompare("video_embed") == 0);

    page.on('request', request => {
        if (request.url().includes("manifest.mpd")) {
            console.log("Manifest url: " + request.url());
            if (request.url().includes("prima-vod-prep-sec")) {
                var command = YOUTUBE_DL_PATH + ' -q -o particka_' + pad(part, 3) + ' ' + request.url();
                console.log("Command to run: " + command);
                exec(command, (e, stdout, stderr) => {
                    if (e instanceof Error) {
                        console.error(e);
                        throw e;
                    }
                    console.log('stderr ', stderr);
                    browser.close();
                });
            }
        }
    });
    await videoframe.click(PLAY_VIDEO_BUTTON_SELECTOR);
    await page.waitFor(1000 * 60 * 5);
}

async function downloadAll() {
    for (var i = 1; i < 99; i++) {
        await download(i);
    }
}

downloadAll();
